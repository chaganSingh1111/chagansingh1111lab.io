---
title: Simple Use of File and Directory Path Handling with path.join and path.resolve in Node.js
---


When we work with path the path module in Node.js provides useful methods to handle and manipulate paths. In this blog post, we will explore two important methods: path.join and path.resolve. We will understand their differences and see how they can simplify path handling in our [Node.js](https://nodejs.org/en) projects.Here are some examples given for you to understand it more clearly.


![NODE JS](../images/Untitled.png)


# Using path.join:
The path.join method is used to concatenate multiple path segments into a single path. It intelligently handles the directory separators based on the underlying operating system. Let's see an example:

### Example:
``` bash
$ const path = require('path');

const directory = 'my-folder';
const filename = 'my-file.txt';

const filePath = path.join(directory, filename);
console.log(filePath);
```


### Output:

``` bash
$ my-folder/my-file.txt
```


## Reason:
In the example above, we first import the path module. We then define two path segments: directory and filename. By calling path.join(directory, filename), we combine these segments into a single path. The resulting path is my-folder/my-file.txt, where the directory separator (/ or \) is automatically handled based on the operating system Like linux and windows.

More info: [path.join](https://www.w3schools.com/nodejs/met_path_join.asp)



### Extra Example Practice:

``` bash
$ var path = require('path');

var x = path.join('Users', 'Refsnes', 'demo_path.js');

console.log(x); 
```



# Using path.resolve:

The path.resolve method is used to resolve an absolute path from the given path segments. It provides a convenient way to generate an absolute path relative to the current working directory. Let's look at an example:

![PATH](../images/Untitled2.png)

### Example:
``` bash
$ const path = require('path');

const relativePath = 'my-folder';
const absolutePath = path.resolve(relativePath);
console.log(absolutePath);
```


### Output:

``` bash
$ /Users/username/my-folder
```


## Reason:

In the example above, we import the path module and define a relativePath variable. By calling path.resolve(relativePath), we generate the absolute path by resolving relativePath relative to the current working directory. The resulting path is an absolute path specific to the operating system on which the code is executed.
More info: [path.resolve](https://dev.to/ifeanyichima/pathresolve-1ime)


### Extra Example Practice:

``` bash
$ const path = require('path');
   
console.log("Current directory:", __dirname);
   
// Resolving 2 path-segments
// with the current directory
path1 = path.resolve("users/admin", "readme.md");
console.log(path1)
   
// Resolving 3 path-segments
// with the current directory
path2 = path.resolve("users", "admin", "readme.md");
console.log(path2)
   
// Treating of the first segment
// as root, ignoring the current directory
path3 = path.resolve("/users/admin", "readme.md");
console.log(path3) 
```

# Benefits and Use Cases:

* Both path.join and path.resolve handle directory separators appropriately based on the underlying operating system. This ensures code portability across different platforms.
* These methods provide a clean and straightforward way to concatenate and resolve paths, reducing manual string manipulation and potential errors.
* path.join helps in constructing paths by joining directories, while path.resolve resolves a path to an absolute path, making it useful for determining the exact location of a file or directory.



# Conclusion:

In this blog post, we explored the usage of two important methods from the path module in Node.js: path.join and path.resolve. We saw how path.join simplifies path concatenation by handling directory separators automatically. Additionally, path.resolve helps generate absolute paths relative to the current working directory. By utilizing these methods, developers can streamline path handling and ensure compatibility across different operating systems.

Understanding and effectively using path.join and path.resolve in your Node.js projects will make working with file and directory paths more intuitive and efficient.when we use fs(file system) we mostly work with path.join and sometimes we may need path.resolve.




Created By : Chagan Singh